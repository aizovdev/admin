PHP Version Required : 7.3 Node composer

Step 1: git clone -b master url

Step 2: Rename .env.example to .env

Step 3: connect mysql in env

Step 4: composer install

Step 5: npm install

Step 6: php artisan migrate

Step 7: php artisan db:seed --class=DatabaseSeeder

Step 8: php artisan key:generate

Step 9: php artisan passport:install

Step 10: npm run prod

Step 11: php artisan serve

Backendframework :https://laravel.com/ Front-end : https://vuejs.org/